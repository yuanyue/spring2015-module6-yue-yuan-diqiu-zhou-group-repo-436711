var app = require('http').createServer(handler);
var io = require('socket.io')(app);
var fs = require('fs');
var roomlist = [];
var users = [];


app.listen(3000);

function findIndexUsers(usrname, users){
	for(i =0; i<users.length; i++){
		if(users[i].username == usrname){
			return i;
		}
	}
	return -1;
}



function handler (req, res) {
  fs.readFile(__dirname + '/login.html',
  function (err, data) {

    if (err) {
      res.writeHead(500);
      return res.end('Error loading index.html');
    }

    res.writeHead(200);
    res.end(data);
  });
}

io.on('connection', function(socket){
	console.log('connecting socket id= '+socket.id);////////////////////////
	// Log in
	socket.on('username', function(data){
		console.log("username: " + data.username);
		console.log(socket.id);
		users[data.username]={username: data.username, id: socket.id, socket: socket};
		socket.emit("username_to_client", {"username": data.username});
	});
	//create a new private room
	socket.on('createPrivateRoom',function(data){
		if(roomlist[data.roomName]==undefined){
			roomlist[data.roomName]={creator: data.creator, banner: "Not applicable", password: data.password};
			socket.join(data.roomName);
			users[data.creator].socket.emit("privateRoom_to_client", {
				"created":true,
				"room": data.roomName,
				"creator": data.creator
			});
		}else{
			io.sockets.emit("room_to_client",{
				"created":false,
				"room": "Room already created"
			});
		}
	});
	//join private room;
	socket.on('joinPrivateRoom', function(data){

		if(roomlist[data.roomName]!=undefined){
			if(roomlist[data.roomName].banner!=data.username){
				if(roomlist[data.roomName].password==data.password){
					users[data.username].socket.emit('joinPrivateRoom_to_client',{"exist":true,"permitted":true});
				}else{
					users[data.username].socket.emit('joinPrivateRoom_to_client',{"exist":true,"permitted":false});
				}
			}else{
				users[data.username].socket.emit('joinPrivateRoom_to_client',{"exist":true,"permitted":false})
			}
		}else{
			users[data.username].socket.emit('joinPrivateRoom_to_client',{"exist":false, "permitted":false});
		}
		});

	//Create a new room;
	socket.on('createroom',function(data){
		socket.join(data.roomName);
		if(roomlist[data.roomName]== undefined){
			roomlist[data.roomName]={creator: data.creator, banner: "Not applicable"};
			console.log(roomlist[data.roomName].creator);
			users[data.creator].socket.emit("room_to_client", {
				"room": data.roomName,
				"creator": data.creator
			});
		}else{
			users[data.creator].socket.emit("room_to_client",{
				"room": "room already created"
			});
		}
	});
	//start to kick someone
	socket.on('kick', function(data){
		console.log(roomlist[data.roomName].creator);
		console.log(data.creator);
		if(roomlist[data.roomName].creator==data.creator){
			users[data.creator].socket.emit("kick_to_client",{
				"kickedUser": data.kickedUser,
				"roomName": data.roomName,
				"creator": data.creator,
				"message": "yes"
			});
			console.log(users[data.creator].id);
			console.log('yes');
		}else{
			users[data.creator].socket.emit("kick_to_client",{
				"kickedUser": data.kickedUser,
				"roomName": data.roomName,
				"creator": data.creator,
				"message": "no"
			});
			console.log('no');
		}
	});
	//kick someone after the client send the callback
	socket.on('kickedUser', function(data){
		if(users[data.username]!=undefined){
			users[data.username].socket.emit('kickSuccess',{"kicked":true,"kickedUser":data.username});
			users[data.username].socket.leave(data.room);
		}
	});
	//ban the user from going into some room
	socket.on('ban', function(data){
		console.log("hear");
		console.log(roomlist[data.roomName].creator);
		console.log(data.creator);
		if(roomlist[data.roomName].creator==data.creator){
			console.log("sending");
			roomlist[data.roomName].banner = data.banner;
			users[data.creator].socket.emit("ban_to_client", {
				"message": true,
				"banner": data.banner
			});
		}
	});

	socket.on('ban_to_server',function(data){
		users[data.banner].socket.emit("banned",{
			message: true});
	});
	//send private message

	socket.on('privateMsg', function(data){
		socket.broadcast.to(users[data.target].id).emit('privateMsg_client', {
			message: data.message,
			author: data.author
		});
	});
	//join a room;
	socket.on('joinRoom', function(data){
		console.log('join');
		if(roomlist[data.roomName]!=undefined){
			console.log("c");
			if(roomlist[data.roomName].banner!=data.username){
				////////////////////////////////to be completed
				console.log("send");
				users[data.username].socket.emit('joinroom_to_client',{"exist":true,"permitted":true});
			}else{
				users[data.username].socket.emit('joinroom_to_client',{"exist":true,"permitted":false})
			}
		}else{
			users[data.username].socket.emit('joinroom_to_client',{"exist":false, "permitted":false});
		}
		});
	//Send message
	/*socket.on('message',function(data){
		console.log("message: "+data.message);
		io.sockets.emit("message_to_client",{"message":data.message});*/
   socket.on('chat message', function(data){
   	console.log('msg emitted');
   	if(roomlist[data.roomName].creator==data.author){
   		io.emit('chat_message_to_client', {
    	"message" : data.message,
    	"author" : data.author,
    	"creator" : "yes"});
   	}else{
   		io.emit('chat_message_to_client', {
    	"message" : data.message,
    	"author" : data.author,
    	"creator" : "no"});
   	}
   });
    
    
    //switch admin property
    socket.on('switchAdmin',function(data){
    	if(roomlist[data.roomName].creator==data.creator){
    		console.log("creator: "+ roomlist[data.roomName].creator);
    		roomlist[data.roomName].creator=data.targetUser;
    		console.log(roomlist[data.roomName].creator);
    		users[data.creator].socket.emit('switchAdmin_to_client',{"message":"success"});
    	}
    });
    //quit room
    socket.on('quitRoom',function(data){
    	users[data.user].socket.leave(data.roomName);
	});
});